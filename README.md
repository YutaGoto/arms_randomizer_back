# ARMS Randomizer

ARMSのARMをランダムでピックアップしてくれます

## Setup

Migrationタスクを実行する

```sh
diesel migration run --database-url ./db/db.sqlite
```

DBをリセットするときは redo する

```sh
diesel migration redo --database-url ./db/db.sqlite
```

Seedデータを挿入する

```sh
sqlite3 db/db.sqlite < db/seed.sql
```

## サーバの起動

```sh
cargo run
```

## ローカル環境でのランダムアームのアクセス

```sh
curl http://localhost:8000/random_arms
```

## テスト

```sh
cargo test
```
