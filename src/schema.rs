table! {
    arms (id) {
        id -> Nullable<Integer>,
        name -> Text,
        name_en -> Text,
        description -> Text,
        image_url -> Bool,
    }
}
