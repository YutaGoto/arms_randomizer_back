extern crate rand;

use diesel::{self, prelude::*};
use rand::seq::SliceRandom;

mod schema {
    table! {
        arms {
            id -> Nullable<Integer>,
            name -> Text,
            name_en -> Text,
            description -> Text,
            image_url -> Text,
        }
    }
}

use self::schema::arms;
use self::schema::arms::dsl::{arms as all_arms};

#[table_name="arms"]
#[derive(Serialize, Queryable, Insertable, Debug, Clone)]
pub struct Arm {
    pub id: Option<i32>,
    pub name: String,
    pub name_en: String,
    pub description: String,
    pub image_url: String
}

impl Arm {
    pub fn all(conn: &SqliteConnection) -> Vec<Arm> {
        all_arms.order(arms::id.asc()).load::<Arm>(conn).unwrap()
    }

    pub fn random_arms(conn: &SqliteConnection) -> Vec<Arm> {
        let mut rng = rand::thread_rng();
        let mut arms_list = all_arms.order(arms::id.asc()).load::<Arm>(conn).unwrap();
        arms_list.shuffle(&mut rng);
        arms_list.drain(..39);
        return arms_list
    }
}
